#! /bin/bash
#Validar si existeix un argument
#mostrar per missatge si es un directori o no o que es
#------------------------------------
ERR_NARGS=1
OK=0

# si no tiene 1 args, plegar
if [ $# -ne 1 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh arg1[nota]"
    exit $ERR_NARGS
fi

#mirar si es ayuda el argumento
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "Mostramos la ayuda"
    exit $OK
fi

# mirar si es un directorio
directori=$1

if [ -d $directori ]
then
    echo "$directori es un directorio"
elif [ -h $directori ]
then
    echo "$directori es un link"
elif [ -f $directori ]
then
    echo "$directori es un file"   
elif ! [ -e $directori ]
then
    echo "$directori no existe"
else
    echo "$directori es otra cosa"
fi

exit $OK

