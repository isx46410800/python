#! /bin/bash
# Miguel Amoros @j02
# ejercicios basicos
#-----------------------------
#10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
MAX=$1
cont=1
while read -r line && [ $cont -le $MAX ]
do
    echo "$cont: $line"
    cont=$((cont+1))

done
exit 0

#9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
while read -r user
do
    egrep "^$user:" /etc/passwd > /dev/null
    if [ $? -eq 0 ]
    then
        echo "$user es un usuario del sistema"
    else
        echo "$user no es un usuario del sistema" >> /dev/stderr
    fi
done
exit 0

#8. Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
for user in $*
do
    egrep "^$user:" /etc/passwd > /dev/null
    if [ $? -eq 0 ]
    then
        echo "$user es un usuario del sistema"
    else
        echo "$user no es un usuario del sistema" >> /dev/stderr
    fi
done
exit 0
#7. Processar línia a línia l’entrada estàndard, si la línia té més de 10 caràcters la mostra, si no no.
while read -r line
do
    egrep "^[^ ]{10,}"
done
exit 0


#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra  quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana genera un error per stderr.
#Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
laborables=''
festivos=''
for dia in $*
do
    case $dia in
        'lunes'|'martes'|'miercoles'|'jueves'|'viernes')
            echo "$dia es laboral"
            laborables="$laborables $dia";;
        'sabado'|'domingo')
            echo "$dia es festivo"
            festivos="$festivos $dia";;
        *)
            echo "$dia no es un dia correcto" >> /dev/stderr
    esac
done
echo "festivos: $festivos"
echo "laborables: $laborables"
exit 0

#5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
while read -r line
do
    echo $line | cut -c1-5
done
exit 0

#4. Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
for mes in $*
do
    case $mes in
        '2')
            echo "$mes tiene 28 dias";;
        '1'|'3'|'5'|'7'|'8'|'10'|'12')
            echo "$mes tiene 31 dias";;
        '2'|'4'|'6'|'9'|'11')
            echo "$mes tiene 30 dias";;
        *)
            echo "$mes no es un mes correcto"
    esac
done
exit 0

#3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
cont=0
MAX=$1
while [ $cont -le $MAX ]
do
    echo $cont
    cont=$((cont+1))
done
exit 0

#2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
count=0
for arg in $*
do
    count=$((count+1))
    echo "$count: $arg"
done
exit 0

#1. Mostrar l’entrada estàndard numerant línia a línia
count=0
while read -r line
do
    count=$((count+1))
    echo "$count: $line"
done

exit 0