#! /bin/bash
#@j02 Miguel Amorós
#12-for-nota.sh nota[...] -> una o mes notes
#Pasar-hi notes i dir si esta suspes o no
#---------------------------------------------------
# si no tiene 1 args, plegar
if [ $# -lt 1 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh arg1[nota]..."
    exit 1
fi

#mirar si es ayuda el argumento
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "Mostramos la ayuda"
    exit 0
fi

# bucle for para iterar notas
listanotas=$*

for nota in $listanotas
do
    # la nota tiene que ser un nota valida 0-10
    if [ $nota -lt 0 -o $nota -gt 10 ]
    then
        echo "error, nota incorrecte" >> /dev/stderr
        echo "USAGE: prog.sh nota[0-10]" >> /dev/stderr
    elif [ $nota -lt 5 ]
    then
        echo "Has suspendido con un $nota"
    elif [ $nota -ge 5 -a $nota -lt 7 ]
    then
        echo "Tienes un aprobado con un $nota"
    elif [ $nota -ge 7 -a $nota -lt 9 ]
    then
        echo "Tienes un notable con un $nota" 
    else
        echo "Tienes un execelente con un $nota"
    fi

done

exit 0