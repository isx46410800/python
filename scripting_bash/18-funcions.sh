#! /bin/bash
#miguel amoros
#funciones
#funcion usuario
#----------------------------------------------------------
#funcion que me dan un login y me tiene que dar el gname(gid)
function showUserGroup(){

	#ayuda
	if [ $# -eq 1 -a "$1" = "-h" -o "$1" = "--help" ]
	then
		echo "mostramos ayuda"
		return $OK
	fi

	ERR_NARGS=1
	ERR_NOLOGIN=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "ERR: num args incorrecte"
		echo "USAGE: $0 login"
		return $ERR_NARGS
	fi

	#validar si existe el login
	login=$1

	Line=$(egrep "^$login:" /etc/passwd 2> /dev/null)
	if [ $? -ne 0 ]
	then
		echo "ERR: el login $login no existe"
		return $ERR_NOLOGIN
	fi

	#buscamos el gid
	gid=$(echo $Line | cut -d: -f4)

	gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)

	echo "LOGIN: $login"
	echo "GNAME(GID): $gname($gid)"

	return $OK
}

#funcion que me dicen un nombre de grup(gname) y decir todos sus campos
function showGroup(){
	ERR_NARGS=1
	ERR_NOGNAME=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "ERR: num args incorrecte"
		echo "USAGE: $0 gname"
		return $ERR_NARGS
	fi

	#validar si existe el login
	gname=$1

	groupLine=$(egrep "^$gname:" /etc/group 2> /dev/null)
	if [ $? -ne 0 ]
	then
		echo "ERR: el gname $gname no existe"
		return $ERR_NOGNAME
	fi

	gid=$(echo $groupLine | cut -d: -f3)
	userList=$(echo $groupLine | cut -d: -f4)

	echo "GNAME: $gname"
	echo "GID: $gid"
	echo "UserLISTS: $userList"

	return $OK
}

#funcion que te dice un login y te dice el gecos y todos sus campos
function showGecos(){

	ERR_NARGS=1
	ERR_NOLOGIN=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "ERR: num args incorrecte"
		echo "USAGE: $0 login"
		return $ERR_NARGS
	fi

	#validar si existe el login
	login=$1

	userLine=$(egrep "^$login:" /etc/passwd 2> /dev/null)
	if [ $? -ne 0 ]
	then
		echo "ERR: el login $login no existe"
		return $ERR_NOLOGIN
	fi
	
	#mostrar
	gecos=$(echo $userLine | cut -d: -f5)

	name=$(echo $gecos | cut -d, -f1)
	office=$(echo $gecos | cut -d, -f2)
	phoneOffice=$(echo $gecos | cut -d, -f3)
	phoneHome=$(echo $gecos | cut -d, -f4)

	echo "GECOS: $gecos" 
	echo "name: $name" | sed -r 's/(^.*$)/\t\1/g'
	echo "office: $office" | sed -r 's/(^.*$)/\t\1/g'
	echo "Ophone: $phoneOffice" | sed -r 's/(^.*$)/\t\1/g'
	echo "phoneHome: $phoneHome" | sed -r 's/(^.*$)/\t\1/g'
	return 0
	
}

#funcion que te da un login y te dice todos sus campos
function showUser(){
	ERR_NARGS=1
	ERR_NOLOGIN=2
	OK=0

	#validar args
	if [ $# -ne 1 ]
	then
		echo "ERR: num args incorrecte"
		echo "USAGE: $0 login"
		return $ERR_NARGS
	fi

	#validar si existe el login
	login=$1

	userLine=$(egrep "^$login:" /etc/passwd 2> /dev/null)
	if [ $? -ne 0 ]
	then
		echo "ERR: el login $login no existe"
		return $ERR_NOLOGIN
	fi
	
	#mostrar
	uid=$(echo $userLine | cut -d: -f3)
	gid=$(echo $userLine | cut -d: -f4)
	gecos=$(echo $userLine | cut -d: -f5)
	home=$(echo $userLine | cut -d: -f6)
	shell=$(echo $userLine | cut -d: -f7)

	echo "uid: $uid"
	echo "gid: $gid"
	echo "gecos: $gecos"
	echo "home: $home"
	echo "shell: $shell"

	return $OK
}

function multiplicar(){
	echo "$(($1*$2))"
	return 0
}
function sumar(){
	echo "$(($1+$2))"
	return 0
}
function hola(){
	echo "hola funciones"
	return 0
}
