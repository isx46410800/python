#! /bin/bash
#--------------------------------------------------------------------------------------
#10) Programa: prog.sh
#    Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#wireshark:x:976:guest
while read -r gid #(line)
do	
	groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group) #lo que entramos será el gid que será la entrada estandar stdin del fichero etc/group
	#todo esto queda metido en una variable

	gname=$(echo $groupLine | cut -d: -f1) #hacemos un echo del contenido de la variable para poder recortarlo
	
	listusers=$(echo $groupLine | cut -d: -f4)

	echo "gname: $gname, gid: $gid, users: $listusers"
done
exit 0

#9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#bucle para iterar cada opcion y ver cada cosa por separado
cognoms=''
edat=''
opcions=''
arguments=''
#mientras no sea nulo el $1
while [ -n "$1" ]
do
	case $1 in
		'-r'|'-m'|'-j')
			opcions="$opcions $1";;
		'-c')
			cognoms=$2 #vemos que el $2 sera lo que debemos guardarnos
			shift;;
		'-e')
			edat=$2
			shift;;
		*)
			arguments="$arguments $1" #acumulamos en el contador de argumentos cuando son letras
	esac
	shift #ponemos shift para que vaya saltando los argumentos hasta que sea nulo
done
echo "les opcions son $opcions"
echo "els arguments son $arguments"
echo "els cognoms son $cognoms"
echo "les edats son $edat"
exit $status

# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
#   Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
cont=0
for file in $*
do
	if [ -f $file ] #si el file es un regular file hacemos lo siguiente
	then
		gzip $file &> /dev/null #echo "accion comprimir"
		#si esto se ha hecho bien
		if [ $? -eq 0 ] #ok: nombre del archivo mas contador de archivos comprimidos
		then
			echo $file
			cont=$((cont+1))
		else #no ok: mensaje de error
			echo "Ha anat malamente el $file" >> /dev/stderr #por cada error lo vamos acumulando en los stderror
			status=2 #indicamos que el status de error es 2
		fi
	else #sino es un regular file
		echo "error: no es un file $file"
		status=2
	fi
done
exit $status

#3)3) Processar arguments que són matricules:
# a) llistar les vàlides, del tipus: 9999 AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)
status=0
while read -r matricula
do
    echo $matricula | egrep "^[0-9]{4} [A_Z]{3}$"
    if [ $? -ne 0 ]
    then
        echo "$matricula no es una matricula correcta" >> /dev/stderr
        status=$((status+1))
    fi
done
exit $status

#2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.
count=0
for arg in $*
do
    echo $arg | egrep ".{3,}"
    if [ $? -eq 0 ]
    then
        count=$((count+1))
    fi
done
echo "hay $count de 3 o mas"
exit 0

#1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
for arg in $*
do
    echo $arg | egrep ".{4,}"
done
exit 0