#! /bin/bash
# Miguel Amoros @j02
#-----------------------------

# processar gnames rebuts per #arguments i mostrar per a cada un el llistat
#dels usuaris del passwd que pertanyen a grup.
for gname in $*
do
    groupLine=$(egrep "^$gname:" /etc/group)
    if [ $? -eq 0 ]
    then
        gid=$(echo $groupLine | cut -d: -f3)
        egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd 
    else
        echo "$gname no es un gname del sistenma" >> /dev/stderr
    fi
done
exit 0

#1.processar stdin login i mostrar login, uid i gid
while read -r login
do
    loginLine=$(egrep "^$login:" /etc/passwd)
    if [ $? -eq 0 ]
    then
        uid=$(echo $loginLine | cut -d: -f3)
        gid=$(echo $loginLine | cut -d: -f4)
        echo "$login $uid $gid"
    else
        echo "$login no es un login del sistema" >> /dev/stderr
        exit 1
    fi
done
exit 0