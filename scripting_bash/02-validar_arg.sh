#! /bin/bash
# Validar que té dos arguments i mostrar-los
# si num args no es correcte, plegar
#------------------------------------

# indica el numero de argumentos pasados
echo $#

# si no tiene 2 args, plegar
if [ $# -ne 2 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh arg1 arg2"
    exit 1
fi

# mostrar los args
echo "Los argumentos son $1 y $2"

exit 0

