#! /bin/bash
#Validar una nota i dir si la nota es aprovat,suspès, notable, excel·lent
#------------------------------------

# si no tiene 1 args, plegar
if [ $# -ne 1 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh arg1[nota]"
    exit 1
fi

# la nota tiene que ser un nota valida 0-10
if [ $1 -lt 0 -o $1 -gt 10 ]
then
    echo "error, nota incorrecte"
    echo "USAGE: prog.sh nota[0-10]"
    exit 2
fi


# si es menos de 5 suspendido
if [ $1 -lt 5 ]
then
    echo "Has suspendido con un $1"
elif [ $1 -ge 5 -a $1 -lt 7 ]
then
    echo "Tienes un aprobado con un $1"
elif [ $1 -ge 7 -a $1 -lt 9 ]
then
    echo "Tienes un notable con un $1" 
else
    echo "Tienes un execelente con un $1"
fi

# mostrar los args
echo "La nota es $1"

exit 0

