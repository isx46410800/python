#! /bin/bash
#Validar si existeix un argument
#mostrar per missatge si es un directori o no
#------------------------------------

# si no tiene 1 args, plegar
if [ $# -ne 1 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh arg1[nota]"
    exit 1
fi

#mirar si es ayuda el argumento
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "Mostramos la ayuda"
    exit 0
fi

# mirar si es un directorio
directori=$1

if [ -d $directori ]
then
    echo "$directori es un directorio"
else
    echo "$directori no es un directorio"
fi

exit 0

