#! /bin/bash
#Validar si existeix un argument
#pasaria un mes i indicar si el mes té 28/30/31
#------------------------------------
ERR_NARGS=1
OK=0
# si no tiene 1 args, plegar
if [ $# -ne 1 ]
then
    echo "Num de args incorrecte"
    echo "USAGE: prog.sh mes"
    exit $ERR_NARGS
fi

#mirar si es ayuda el argumento
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "Mostramos la ayuda"
    exit $OK
fi

# validar que el arg sea un mes
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
    echo "no es un mes"
    echo "USAGE: prog.sh [1-12]"
    exit $ERR_NARGS
fi
# si mes tiene 28-29-30
case $mes in
    '1'|'3'|'5'|'7'|'8'|'10'|'12')
        echo "$mes tiene 31 dias";;
    '4'|'6'|'9'|'11')   
         echo "$mes tiene 30 dias";;
    '2')
        echo "$mes tiene 28 dias";;
    *)
        echo "Aixo no es un mes";;
esac
exit 0