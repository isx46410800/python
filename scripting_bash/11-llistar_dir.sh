#! /bin/bash
#llistar dir
#fa un 'ls' del directori rebut
#verificar 1 arg, i que es un dir
#enumerant cada linia una sota el altre
#de cada cosa que imprime decir si es un regular file, dir, link o altre cosa

#------------------------------------

# verificar 1 arg
if [ $# -ne 1 ]
then
    echo "error num args"
    echo "USAGE: prog.sh arg[dir]"
    exit 1
fi
#help
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "mostramos ayuda"
    exit 0
fi
# verificar que es un dir
if ! [ -d $1 ]
then
    echo "error arg no es un dir"
    echo "USAGE: prog.sh arg[dir]"
    exit 2
fi

#hacer un ls
ls $1

#enumerant cada linia una sota el altre
dir=$1
llistadir=$(ls $dir)
count=1
for file in $llistadir
do
#de cada cosa que imprime decir si es un regular file, dir, link o altre cosa
    if [ -d $dir/$file ]
    then
        echo "es un directorio"
        echo "$count: $file"
    elif [ -f $dir/$file ]
    then
        echo "es un file"
        echo "$count: $file"
    elif [ -d $dir/$file ]
    then
        echo "es una altra cosa"
        echo "$count: $file"
    fi
    count=$((count+1))
done

exit 0