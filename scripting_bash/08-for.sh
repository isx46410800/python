#! /bin/bash
#exemples for
#------------------------------------
#contador
cont=0

#iterar pels elements el resultat de una ordre
llista=$(ls) #comand sustituion,mete en la variable el resultado de la comanda
#llista="pere miguel pau"
for nom in $llista #in "pere" "pau" "miguel"
do
       	echo $nom
done
exit 0



# para iterar los argumentos pasados
for arg in $*
do
    cont=$((cont + 1))
    echo "arg $cont: $arg"
done
exit 0

# para iterar la lista de argumentos
for arg in $@
do
    echo " $arg"
done
exit 0

