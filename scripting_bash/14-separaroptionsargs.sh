#! /bin/bash
#@j02 Miguel Amorós
#
#Exercici 16- validar arguments
#prog -h o --help
#prog -a arg
#prog -b -a arg
#cas1 arg // cas2 arg
#----------------------------------------------------------------------------

# verificar 1 arg
if [ $# -lt 1 ]
then
    echo "error num args"
    echo "USAGE: prog -h o --help#prog -a arg#prog -b -a arg#cas1 arg // cas2 arg"
    exit 1
fi
#help
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "mostramos ayuda"
    exit 0
fi

#separar opciones y argumentos
opciones=''
argumentos=''

for element in $*
do
    case $element in
        '-a'|'-b') 
            opciones="$opciones $element";;
        *)  
            argumentos="$argumentos $element"
    esac
done
echo "opciones: $opciones"
echo "argumentos: $argumentos"
exit 0