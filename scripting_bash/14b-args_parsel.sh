#! /bin/bash
#@j02 Miguel Amorós
#prog -a file -b -c -d num -e arg[...]
#-----------------------------------------------------------------

# verificar 1 arg
if [ $# -lt 1 ]
then
    echo "error num args"
    echo "USAGE: prog -h o --help#prog -a arg#prog -b -a arg#cas1 arg // cas2 arg"
    exit 1
fi
#help
if [ $1 == "--help" -o $1 == "-h" ]
then
    echo "mostramos ayuda"
    exit 0
fi

#Mientas haya argumento1, printa el valor $1 y hacemos salto(shift)
#separar opciones y argumentos
opciones=''
argumentos=''
numeros=''
files=''

while [ -n "$1" ]
do
    case $1 in
        '-a') 
            files=$2
            shift;;
        '-c'|'-b'|'-e') 
            opciones="$opciones $1";;
        '-d') 
            numeros=$2
            shift;;
        *)  
            argumentos="$argumentos $1";;
    esac
    shift
done
echo "opciones: $opciones"
echo "argumentos: $argumentos"
echo "numeros: $numeros"
echo "files: $files"
exit 0