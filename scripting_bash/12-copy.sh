#! /bin/bash
#copy.sh
#fa un 'ls' del directori rebut
#copy.sh file[...] directori-desti
#validar args
#validar desti es un dir
#copiar los ficheros que pasemos a un directorio destino
#------------------------------------

# verificar args
if [ $# -lt 2 ]
then
    echo "error num args"
    echo "USAGE: prog.sh file[...] directori-desti" | tr -s '[a-z]' 'A-Z'
    exit 1
fi

#files y dir a copiar
llistafiles=$(echo $* | cut -d' ' -f1-$(($#-1)))
dirdesti=$(echo $* | cut -d' ' -f$#)
#comprobar que sea un directorio
if ! [ -d $dirdesti ]
then
    echo "error, no es un dir desti correcto"
    exit 2
fi
# para cada file validada lo copiamos al dir
for file in $llistafiles
do 
    if ! [ -f $file ]
    then
        echo "error, no es un file" >> /dev/stderr
        exit 3
    fi
    cp $file $dirdesti/.
done

exit 0



