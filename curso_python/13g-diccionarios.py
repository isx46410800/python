# !/usr/bin/python3
# -*-coding: utf-8-*-

gente = {'1': "miguel", '2': "cristina", '3': "isabel"}
gente['4'] = 'maria'

for k in gente:
    print(k)

for k,v in gente.items():
    print(f'key: {k}   valor: {v}')

for k in gente.keys():
    print(f'key: {k}')

for v in gente.values():
    print(f'valor: {v}')

