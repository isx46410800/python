# !/usr/bin/python3
# -*-coding: utf-8-*-

a = True
b = False
x = ('perro', 'gato', 'cerdo')
y = 'gato'

# comparamos a y b
if a and b:
    print('Expresiones son TRUE')
else:
    print("Expresiones son False")

# comparamos algo de una tupla con y
if x[1] and y:
    print('Expresiones son TRUE')
else:
    print("Expresiones son False")
# o lo mismo
if y in x:
    print('Expresiones son TRUE')
else:
    print("Expresiones son False")

# otra comparacion
if a is not y:
    print('Expresiones son TRUE')
else:
    print("Expresiones son False")