# !/usr/bin/python3
# -*-coding: utf-8-*-

# lista
x = [1,2,3,4]

# cosas con listas
print("##LISTAS##")
print(x[2])
x[2] = 10
for i in x:
    print(i)

# tupla
print("##TUPLAS##")
t = (1,2,3,4,5)

# cosas con tuplas
## t[2] = 10 NO SE PUEDE ASIGNAR PARA CAMBIAR
print(t[2])
for e in t:
    print(e)

# rangos ( no incluido ultimo elemento)
print("##RANGOS##")
r = range(5) # no se puede asignar sino es con una lista
ra = list(range(5))
ra[2] = 20
rang = range(5,10,2) # del 5 al 10 de dos en dos
# cosas con rangos
for e in ra:
    print(e)

for e in rang:
    print(e)

# diccionarios
print("##DICCIONARIOS##")
dic = { 'x' : 5, 'y' : 'miguel', 'z' : False }

# cosas con diccionarios
print(dic['y'])
dic['y'] = 'miguelito'
for id, valor in dic.items():
    print(f"id: {id} valor: {valor}")

for e in dic.values():
    print(e)

for e in dic:
    print(f'el id es {e}')
    print(f'el valor es {dic[e]}')