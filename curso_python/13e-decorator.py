# !/usr/bin/python3
# -*-coding: utf-8-*-

# definimos un decorador: una funcion que llama a otro funcion
def fi(f):
    def fi2():
        print("Hello World!")
        f()
        print("Hello World!")
    return fi2

@fi
def fi3():
    print("Bye bye!")

fi3()

# resultado
# Hello World! primer fi con fi2, luego fi3 luego vuelve f()
# Bye bye!
# Hello World!