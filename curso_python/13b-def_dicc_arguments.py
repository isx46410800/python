# !/usr/bin/python3
# -*-coding: utf-8-*-

# definimos una funcion con una lista de argumentos
def main():
    people(hijo="miguel", padre="papa", madre="mama", hija="cristina")


# funcion que por cada argumento que haya, lo printe
def people(**kwargs): # multiples leywords args pasados por dicc
    if len(kwargs):
        for name in kwargs: #para la clave de el dicc kwargs
            print(f'clave: {name} y su valor: {kwargs[name]}')
    else:
        print("nobody")

if __name__ == "__main__":
    main()