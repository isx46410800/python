# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos libreria para tratar con decimales
from decimal import *

# diferentes data types
w = 40.0
x = 75 //  7
y = "miguel"
z = True
mayus = "hello world".upper()
minus = "hello world".lower()
capi = "hello world".capitalize()
spaces = '''
hi

miguel
'''

# trato con numericos types
a = Decimal('.1')
b = Decimal('.3')
c = a + a + a - b

# trato con booleanos
if z:
    print("True")
else:
    print("False")

# mostramos resultados
print(f"x es {x}")
print("x es {}" .format(x))
print("el orden cambia {0} y {1}" .format(x,w))
print("el orden cambia {1} y {0}" .format(x,w))
print(f"numero total con ceros a la izquierda {1:>03} y derecha {2:<04}")
print(mayus)
print(minus)
print(capi)
print(spaces)
print(a)
print(b)
print(c) # aunque es 0 lo muestra como un float por la libreria decimal

# ver tipo de dato introducido
print(type(w)) #float
print(type(x)) #integer
print(type(y)) #string
print(type(z)) #boolean
print(type(a))
print(type(b))
print(type(c))