# !/usr/bin/python3
# -*-coding: utf-8-*-

def main():
    fileInput = open('lines.txt', 'rt') # r read t text
    fileOutput = open('linesOutput.txt', 'wt') # w write t text

    for line in fileInput:
        print(line.rstrip(), file=fileOutput) # cada linea sin blancos la envia al nuevo file
        print('.', end='', flush=True) # aqui solo printa esto por cada linea leida

    fileOutput.close() # cierra el doc nuevo
    print('\nDone.') # printa que se ha realizado todo

if __name__ == "__main__":
    main()