# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos la libreria platform con sus funciones
import platform

# asignaciones
name = "Miguel"
version = platform.python_version()
x= 100

# sustitucion en frases
print("Hello World, my name is {}" .format(name))
print("My version python is {}" .format(version))
print("My name is", name)
print(x)
print(f"My name is {name}")
print(f'My Python version is {version}')