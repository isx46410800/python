# !/usr/bin/python3
# -*-coding: utf-8-*-

# declaramos la funcion basica sin pasar nada
def main():
    runMe()

def runMe():
    print("I am running!")

# declaramos funcion pasandole algo
def name(n):
    print(f"I am {n}")

def operation(n=25):
    print(n)
    return n*2 # el return no printa nada, solo retona algo

# pasamos a una variable el resultado del return
x = operation()

# pasamos a la funciones dos argumentos
def main2():
    runMe2(5,6)

def runMe2(x,y):
    print("I am running!")
    print(x,y)

# funciones con bucle while
def mainLoop():
    runLoop(0,5)

def runLoop(x,y):
    while x < y:
        print(f"El valor de X es {x}")
        x +=1

# funciones con bucle for
def mainLoop2():
    runLoop2('casa',5, True)

def runLoop2(x,y,z):
    lista = [x,y,z]
    for e in lista:
        print(f"El valor es {e}")

# prueba y error test
if __name__ == "__main__":
    main()
    runMe()
    name("Miguel")
    name(2)
    print(x)
    main2()
    mainLoop()
    mainLoop2()