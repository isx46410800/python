# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos la libreria platform con sus funciones
import platform

food = ["breakfast", "lunch", "snack", "dinner"]
n = 0
m = 0

# bucle basico
while n < 10:
    print(n)
    n +=1

# bucle de una lista
while m < 4:
    print(food[m])
    m +=1

# bucle con for para iterar cada cosa de una lista
for i in food:
    print(i)