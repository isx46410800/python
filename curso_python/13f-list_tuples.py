# !/usr/bin/python3
# -*-coding: utf-8-*-

# LISTA ACCIONES -- TUPLAS SON INMUTABLES Y NO SE PUEDE
def main():
    lista = ['perro', 'gato', 'cerdo', 'caballo']
    lista2 = ['perro', 'gato', 'cerdo', 'caballo']

    print(lista[1]) # gato
    print(lista[1:3]) # gato, cerdo
    print(lista[0:5:2]) # perro, cerdo
    print(lista.index('gato')) # 1, busca la posicion de esa palabra
    lista.append('koala') # añade koala
    lista.insert(0, 'vaca') # añade vaca en posicion 0
    lista.remove("vaca") # borra de la lista vaca
    lista.pop() # borra el ultimo elemento de la lista
    lista.pop(1) # borra esa posicion de la lista
    del lista[1] # borra de la lista ese elemento
    del lista[0:1] # borra ese slicing
    print(len(lista)) # cuenta en numero de elementos de la lista
    lista.extend(lista2) # junta dos listas

    print_lista(lista) # funcion de iterar la lista

# funcion para iterar la lista pasada por argumento
def print_lista(lista):
    for i in lista:
        print(i, end=' ', flush=True)
    print()

# resultados
main()