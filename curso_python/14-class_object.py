# !/usr/bin/python3
# -*-coding: utf-8-*-

# definimos una clase
class mobile:
    #definimos unas variables con contenido
    old_phone = "keypad"
    new_phone = "touch screen"

    # definimos funciones que printes esas variables
    def old_mobile(self):
        print(self.old_phone)
    
    def new_mobile(self):
        print(self.new_phone)

# creamos funcion,variable con objeto y sus dos partes de funciones
def main():
    x = mobile()
    x.old_mobile()
    x.new_mobile()

# resultados
if __name__ == "__main__":
    main()