# !/usr/bin/python3
# -*-coding: utf-8-*-

def main():
    try:
        #x = int("hello")
        y = 5/2
    except ValueError:
        print("No se puede añadir a int un str")
    except ZeroDivisionError:
        print("No se puede dividir entre 0!")
    else:
        print("Unkown")
    print(y)

if __name__ == "__main__":
    main()