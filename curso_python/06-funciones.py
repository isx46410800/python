# !/usr/bin/python3
# -*-coding: utf-8-*-

# declaramos la funcion basica sin pasar nada
def runMe():
    print("I am running!")

# declaramos funcion pasandole algo
def name(n):
    print(f"I am {n}")

def operation(n=25):
    print(n)
    return n*2 # el return no printa nada, solo retona algo

# pasamos a una variable el resultado del return
x = operation()

# prueba y error test
if __name__ == "__main__":
    runMe()
    name("Miguel")
    name(2)
    print(x)