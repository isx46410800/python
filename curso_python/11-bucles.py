# !/usr/bin/python3
# -*-coding: utf-8-*-

x = "123"
y = ""
w = "123"
z = ""
auth = False
count = 0
max_attemp = 5

mascotas = ('perro', 'gato', 'cerdo')

# bucle while escribe algo hasta acertar
while y != x:
    y = input("dime la pass: ")

# bucle for para iterar
for item in mascotas:
    print(item)

# bucle con control
while z != w:
    count +=1
    if count > max_attemp:
        break
    z = input(f"intento {count}-> dime la pass: ")
else:
    auth = True

print("Authorized" if auth else "access denied")
