# !/usr/bin/python3
# -*-coding: utf-8-*-

# definimos una clase de objeto
class Animal:
    def __init__(self, type, name, sound):
        self._type = type
        self._name = name
        self._sound = sound

    def type(self):
        return self._type

    def name(self):
        return self._name

    def sound(self):
        return self._sound

def print_animal(x):
    if not isinstance(x, Animal):
        raise TypeError("error, requiere un animal")
    print(f'El {x.type()} se llama {x.name()} y dice {x.sound()}')

# le pasamos a la funcion de hacer algo, los argumentos al objeto
def main():
    print_animal(Animal("Kitten", "Fluffly", "Meow"))
    print_animal(Animal("Duck", "Donald", "Quak"))

# resultados
if __name__ == "__main__":
    main()