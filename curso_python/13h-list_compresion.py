# !/usr/bin/python3
# -*-coding: utf-8-*-

# de una lista
lista = range(11)
tupla = ((0,1),(1,2),(2,3))

# creas una lista,tupla iterando lista y operaciones
lista2 = [ x * 2 for x in lista]
tupla2 = [ (y*2, x*2) for x,y in tupla]

# resultados
print(lista2)
print(tupla2)