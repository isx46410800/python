# !/usr/bin/python3
# -*-coding: utf-8-*-

# genereamos una funcion que printe del 0 al 9 con separacion
def main():
    for i in range(10):
        print(i, end=' ')
    print()

# generamos una funcion que incluya el rango y siga segun argumentos
def main2():
    for i in inclusive_range(5):
        print(i, end=' ')
    print()

def inclusive_range(*args):
    numargs = len(args)
    start = 0
    step = 1

    # parametros iniciales
    if numargs < 1:
        raise TypeError(f'falta al menos 1, argumentos {numargs}')
    elif numargs == 1:
        stop = args[0]
    elif numargs == 2:
        (start, stop) = args
    elif numargs == 3:
        (start, stop, step) = args
    else:
        raise TypeError(f'hay mas de 3 args, argumentos {numargs}')

    # generador
    i = start
    while i <= stop:
        yield i
        i = i + step



if __name__ == "__main__":
    main()
    main2()