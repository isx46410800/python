# !/usr/bin/python3
# -*-coding: utf-8-*-

# creacion de objetos con funciones dentro
class Hours:
    def morning(self):
        print("I feel fresh")

    def afternoon(self):
        print("I feel bored")

    def evening(self):
        print("I feel tired")

# definimos una funcion donde una variable es un objeto con 3 cosas(funciones definidas)
def main():
    how_i_feel = Hours()
    how_i_feel.morning()
    how_i_feel.afternoon()
    how_i_feel.evening()

class Time:
    h = "horas"
    m = "minutos"
    s = "segundos"
    def hours(self):
        print(self.h)

    def minuts(self):
        print(self.m)

    def seconds(self):
        print(self.s)

def main2():
    how_time = Time()
    how_time.hours()
    how_time.minuts()
    how_time.seconds()

if __name__ == "__main__":
    main()
    main2()