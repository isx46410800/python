# !/usr/bin/python3
# -*-coding: utf-8-*-

def main():
    fileInput = open('cat.jpg', 'rb') # r read b binario
    fileOutput = open('cat_copy.jpg', 'wb') # w write b binario

    # mientras todo se pueda
    while True:
        # leemos datos y lo metemos en un buffer
        buffer = fileInput.read(102400)
        # mientras haya buffer por leer
        if buffer:
            # copiamos del buffer en el file nuevo
            fileOutput.write(buffer)
            print('.', end='', flush=True) # aqui solo printa esto por cada linea leida
        else:
            break

    fileOutput.close() # cierra el doc nuevo
    print('\nDone.') # printa que se ha realizado todo

if __name__ == "__main__":
    main()