# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos modulos de python
import os, datetime, sys

def main():
    # modulo de system
    v = sys.version_info
    print('Mi version es {}.{}.{}' .format(*v))
    # modulo de operating system
    x = os.name
    w = os.getcwdb()
    print(v)
    print(w)
    # modulo de datetime
    date = datetime.datetime.now() # fecha y hora de ahora
    print(date)
    print(date.year)
    print(date.month)
    print(date.day)

# resultados
if __name__ == "__main__":
    main()