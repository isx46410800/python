# !/usr/bin/python3
# -*-coding: utf-8-*-
import requests

# Insertamos palabra a buscar en el diccionario
value = input("Enter Word ")

# buscamos en la url api mas la palabra a buscar
url = 'https://api.dictionaryapi.dev/api/v1/entries/en/' + value

# enviamos peticion de url al navegador 
r = requests.get(url)

# extraemos la palabra in json format 
result = r.json()
print('\n' "*************************************" )

# Evaluamos la palabra encontrada de la bbdd jason
try:
  data = result[0] # de la entrada cogidas, coge la primera (normalmente 1)
  print('Results for :'+ ' ' +  data['word']) ## cogemos el campor word
  print("-------------------------------------")
  print('Meaning') # cogemos el campor meaning
  # cogemos todos los signidicados //getting all meanings with Part of speech
  for means in data['meaning']:
    print('\u0332'.join(means) + ' : ' + data['meaning'][means][0]['definition']+ '\n')
    
    # cogemos todos los ejemplos // getting all examples 
    if 'example' in  data['meaning'][means][0]:
      print('Example : ' + data['meaning'][means][0]['example'] + '\n')

    # cogemos todos los sinonimos //getting all synonyms
    if 'synonyms' in  data['meaning'][means][0]:
      print('Synonyms :')
      for synonyms in data['meaning'][means][0]['synonyms']:
        print(' ' + synonyms + ',' )
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')

  print("-------------------------------------")
  #print('Origin : ' + '\n' + data['origin'])

  # if resoults not found !
except Exception:
  data = result
  print(value, data['title'])
  print(data['message'])


print("*************************************" '\n')