# !/usr/bin/python3
# -*-coding: utf-8-*-

x = 50

# combina operacion en variable con condicion
# si la variable es True hace lo de dentro, sino el ELSE
tengoHambre = True
y = "Necesito comer" if tengoHambre else "solo necesito beber"

print(y)

if x < 50:
    print("es mas pequeño de 50")
elif x == 50:
    print("es igual a 50")
elif x > 50:
    print("es mas grande de 50")
else:
    print("es otra cosa")