# !/usr/bin/python3
# -*-coding: utf-8-*-

# type(x)
# id(x)
# instance(x, tuple or list)

x = (1,'two',3.0,[4],5)
y = (1,'two',3.0,[4],5)
z = [1,'two',3.0,[4],5]
w = 1

# identifica que tipo es la asignacion
print(type(x)) # lista
print(type(x[1])) # string,de la lista
print(type(x[3])) # lista, de la lista

print(id(x)) # un identificador unico
print(id(x[0])) # un identificador unico
print(id(y))

# resultado de que es una tupla
if isinstance(x, tuple):
    print("tuple")
elif isinstance(x, list):
    print("list")
else:
    print("something")

# resultado de que es una lista
if isinstance(z, tuple):
    print("tuple")
elif isinstance(z, list):
    print("list")
else:
    print("something")

# resultado de que es algo
if isinstance(w, tuple):
    print("tuple")
elif isinstance(w, list):
    print("list")
else:
    print("something")