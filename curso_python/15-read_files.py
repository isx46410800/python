# !/usr/bin/python3
# -*-coding: utf-8-*-

def main():
    file = open('lines.txt', 'r')
    # file = open('lines.txt', 'r') # read only
    # file = open('lines.txt', 'w') # write only (empties files)
    # file = open('lines.txt', 'a') # añadir data in files
    # file = open('lines.txt', 'r+') # optional + read or write

    for line in file:
        print(line.rstrip()) #rstrip elimina espacios o lo que se ponga en ()

if __name__ == "__main__":
    main()