# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos la libreria platform con sus funciones
import platform

## Mensaje simple
# print("Mi version de python es la {}" .format(platform.python_version()))

# hacemos una funcion de mensaje de version
def main():
    message()
## el mensaje
def message():
    print("Mi version de python es la {}" .format(platform.python_version()))

# segundo ejemplo
def main2():
    message2()
# el mensaje
def message2():
    print("Uso windows")

# funcion condicional main para hacer las pruebas del codigo
if __name__ == "__main__":
    main()
    main2()
