# !/usr/bin/python3
# -*-coding: utf-8-*-

# definimos una funcion con una lista de argumentos
def main():
    people("miguel", "papa", "mama", "cristina")


# funcion que por cada argumento que haya, lo printe
def people(*args): # multiples argumentos pasados
    if len(args):
        for name in args:
            print(name)
    else:
        print("nobody")

if __name__ == "__main__":
    main()