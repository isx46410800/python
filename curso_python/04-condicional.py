# !/usr/bin/python3
# -*-coding: utf-8-*-

# importamos la libreria platform con sus funciones
import platform

x = 25
y = 15

if x > y:
    print("x is greater than y: where x is {} and y {}" .format(x,y))
elif x == y:
    print("x and y are the same: where x is {} and y {}" .format(x,y))
else:
    print("x is lesser than y: where x is {} and y {}" .format(x,y))